package ru.t1.kupriyanov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kupriyanov.tm.api.service.IAuthService;
import ru.t1.kupriyanov.tm.api.service.IUserService;
import ru.t1.kupriyanov.tm.command.AbstractCommand;
import ru.t1.kupriyanov.tm.exception.user.UserNotFoundException;
import ru.t1.kupriyanov.tm.model.User;

public abstract class AbstractUserCommand extends AbstractCommand {

    //protected IAuthService getAuthService() {
    //    return getServiceLocator().getAuthService();
    //}

    @NotNull
    protected IUserService getUserService() {
        return getServiceLocator().getUserService();
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    public void showUser(@Nullable User user) {
        if (user == null) throw new UserNotFoundException();
        System.out.println(user.getLogin() + " " + user.getEmail());
    }

}
